package br.org.catolicasc.store;


import br.org.catolicasc.store.bean.Cliente;
import br.org.catolicasc.store.bean.ClienteFisico;
import br.org.catolicasc.store.bean.ClienteJuridico;
import br.org.catolicasc.store.bean.ItemPedido;
import br.org.catolicasc.store.bean.Pedido;
import br.org.catolicasc.store.bean.Produto;
import br.org.catolicasc.store.dao.ClienteDao;
import br.org.catolicasc.store.dao.JpaDaoFactory;
import br.org.catolicasc.store.dao.PedidoDao;
import br.org.catolicasc.store.dao.ProdutoDao;

public class Principal {

	public static void main(String args[]) {
		
		ClienteFisico clientef = new ClienteFisico();
		clientef.setNome("Rafael");
		clientef.setEmail("rafael@email.com");
		clientef.setEndereco("Jaragua");
		clientef.setCpf("12345");
		
		ClienteJuridico clientej = new ClienteJuridico();
		clientej.setNome("WEG");
		clientej.setEmail("weg@weg.com.br");
		clientej.setEndereco("Rua da weg");
		clientej.setCnpj("asdasdasdasd");
		
		ClienteDao clienteDao = JpaDaoFactory.getInstance().getClienteDao();
		clienteDao.salva(clientef);
		clienteDao.salva(clientej);
		
		Produto p1 = new Produto();
		p1.setNome("Motor 11443516");
		p1.setValor(485.90f);
		
		Produto p2 = new Produto();
		p2.setNome("10000008");
		p2.setValor(1000000.0f);
		
		ProdutoDao produtoDao = JpaDaoFactory.getInstance().getProdutoDao();
		produtoDao.salva(p1);
		produtoDao.salva(p2);
		
		Pedido pe1 = new Pedido();
		pe1.setCliente(clientej);
		
		ItemPedido ip1 = new ItemPedido();
		ip1.setQtd(1);
		ip1.setPedido(pe1);
		ip1.setProduto(p1);
		
		pe1.addItem(ip1);
		
		PedidoDao pedidoDao = JpaDaoFactory.getInstance().getPedidoDao();
		pedidoDao.salva(pe1);
		
		
		
		System.out.println("--- Clientes --- ");
		for (Cliente cliente : clienteDao.listaTodos()) {
			System.out.println("ID: " + cliente.getId() + " | Nome: " + cliente.getNome());
		}
		
		System.out.println("--- Produtos --- ");
		for (Produto produto : produtoDao.listaTodos()) {
			System.out.println("ID: " + produto.getId() + " | Nome: " + produto.getNome() 
			+ " | Valor: " + produto.getValor());
		}
		
		System.out.println("--- Pedidos --- ");
		for (Pedido pedido : pedidoDao.listaTodos()) {
			System.out.println("ID: " + pedido.getId() + " | Cliente: " + pedido.getCliente().getNome());
			
			double valor = 0.0;
			
			for (ItemPedido itemPedido : pedido.getItens()) {
				System.out.println("ID Item: " + itemPedido.getId());
				System.out.println("Quantidade: " + itemPedido.getQtd());
				System.out.println("Nome: " + itemPedido.getProduto().getNome());
				System.out.println("Valor: " + itemPedido.getProduto().getValor());
				valor += itemPedido.getProduto().getValor()*itemPedido.getQtd();
			}
			System.out.println("Valor total do pedido: " + valor);
		}
	}
	
}
