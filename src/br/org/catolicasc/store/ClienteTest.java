package br.org.catolicasc.store;

import br.org.catolicasc.store.bean.Cliente;
import br.org.catolicasc.store.bean.ClienteFisico;
import br.org.catolicasc.store.bean.ClienteJuridico;
import br.org.catolicasc.store.dao.ClienteDao;
import br.org.catolicasc.store.dao.JpaDaoFactory;

public class ClienteTest {
	
	public static void main(String args[]) {
		ClienteFisico c1 = new ClienteFisico();
		c1.setNome("Jo�o");
		c1.setEmail("joao@hotmail.com");
		c1.setEndereco("Rua do Jo�o, 980");
		c1.setCpf("12345678901");
		
		ClienteJuridico c2 = new ClienteJuridico();
		c2.setNome("Angeloni");
		c2.setEmail("angeloni@angeloni.com.br");
		c2.setEndereco("Rua Bar�o do Rio Branco, 732");
		c2.setCnpj("12345678901234");
		
		ClienteDao dao = JpaDaoFactory.getInstance().getClienteDao();
		dao.salva(c1);
		dao.salva(c2);
	}
}
