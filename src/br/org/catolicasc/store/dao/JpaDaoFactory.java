package br.org.catolicasc.store.dao;

public class JpaDaoFactory {

	public static JpaDaoFactory instance = new JpaDaoFactory();
	
	private JpaDaoFactory() {}
		
	public static JpaDaoFactory getInstance(){
		return instance;
	}
	
	public ClienteDao getClienteDao() {
		return new ClienteDao();
	}
	
	public ProdutoDao getProdutoDao() {
		return new ProdutoDao();
	}
	
	public PedidoDao getPedidoDao() {
		return new PedidoDao();
	}
}
