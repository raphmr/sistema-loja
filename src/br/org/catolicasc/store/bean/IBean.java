package br.org.catolicasc.store.bean;

public interface IBean {

	Long getId();

	void setId(Long id);

}